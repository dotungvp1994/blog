<?php

use Illuminate\Database\Seeder;

use App\Role;
use App\Permission;
class RolesSeeder extends Seeder
{
    private $userPermission = [
        'create-post' => 'Create Post'
    ];

    private $adminPermission = [
        'delete-post' => 'Delete Post',
        'update-post' => 'Update Post',
        'publish-post' => 'Published Post',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userRole = Role::create([
            'name' => 'User',
            'slug' => 'user'
        ]);
        $userPermissions = [];
        foreach ($this->userPermission as $slug => $name) {
            $userPermissions[] = new Permission(['name' => $name, 'slug' => $slug]);
        }
        $userRole->permissions()->saveMany($userPermissions);
        $userPermissions = $userRole->permissions()->pluck('id');
        $adminPermissions = [];
        $adminRole = Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
        ]);
        foreach ($this->adminPermission as $slug => $name) {
            $adminPermissions[] = new Permission(['name' => $name, 'slug' => $slug]);
        }
        $adminRole->permissions()->saveMany($adminPermissions);
        $adminRole->permissions()->attach($userPermissions);
    }
}
