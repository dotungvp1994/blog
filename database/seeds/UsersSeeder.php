<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersSeeder extends Seeder
{
    private $roles = ['admin','user'];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->roles as $role) {
            $user = factory(User::class)->create();
            $user->assignRole($role);
            if( $role == 'admin' ) {
                $this->command->info('Admin login details:');
                $this->command->warn('Username : '.$user->email);
                $this->command->warn('Password : "secret"');
            }
        }
    }
}
