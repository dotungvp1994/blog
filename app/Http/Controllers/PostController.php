<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Requests\Post\CreateRequest;
use App\Http\Requests\Post\UpdateRequest;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::published()->orderBy('published_at','desc')->paginate(1);
        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $data = $request->only('title', 'body');
        $data['slug'] = str_slug($data['title']);
        $data['user_id'] = Auth::user()->id;
        $post = Post::create($data);
        return redirect()->route('edit_post', ['id' => $post->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::published()->findOrFail($id);
        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Post $post)
    {
        $data = $request->only('title', 'body');
        $data['slug'] = str_slug($data['title']);
        $post->fill($data)->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param   id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Post::findOrFail($id);
        $item->delete();
        return back();
    }

    public function drafts()
    {
        $posts = Post::where('published',false)->orderBy('id', 'desc')->paginate();
        return view('posts.manage_draft', compact('posts'));
    }

    public function list()
    {
        $posts = Post::orderBy('id', 'desc')->paginate();
        return view('posts.manage_post', compact('posts'));
    }

    public function publish(Post $post, Request $request)
    {
        $post->published = true;
        $post->published_by = Auth::id();
        if(!empty($request->published_at)) {
            $post->published_at = Carbon::createFromFormat('Y-m-d H:s', $request->published_at)->setTimezone('UTC')->format('Y-m-d H:s');
        } else {
            $post->published_at = Carbon::now('utc')->format('Y-m-d H:s');
        }
        $post->save();
        return back();
    }


    public function myPost(){
        $posts = Post::where('user_id', Auth::id())->orderBy('created_at','desc')->paginate(1);
        return view('posts.my_post', compact('posts'));
    }

    public function myDraft()
    {
        $postsQuery = Post::where('user_id',Auth::id())->unpublished();
        $posts = $postsQuery->paginate();
        return view('posts.my_draft', compact('posts'));
    }
}
