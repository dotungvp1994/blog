<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = [
        'title', 'slug', 'body', 'published', 'published_at', 'user_id', 'published_by'
    ];

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function publisher()
    {
        return $this->belongsTo(User::class, 'published_by');
    }

    public function scopePublished($query)
    {
        $now = Carbon::now('utc')->format('Y-m-d H:s');
        return $query->where('published', true)->where('published_at', '<=', $now);
    }

    public function scopeUnpublished($query)
    {
        $now = Carbon::now('utc')->format('Y-m-d H:s');
        return $query->where('published', false)->orWhere('published_at', '>', $now);
    }

}
