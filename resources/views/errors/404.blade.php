@extends('layouts.app')
@section('title', '401')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    404 | Not Found!
                </div>
            </div>
        </div>
    </div>
@endsection
