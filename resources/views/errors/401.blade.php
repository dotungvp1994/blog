@extends('layouts.app')
@section('title', '401')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                   <div class="row" >
                       <h1  class="text-center" style="padding: 30px;">
                           401 | Not Authorize!
                       </h1>
                       <a class="text-center" href="{{url()->previous()}}" > Back</a>
                   </div>
                </div>
            </div>
        </div>
    </div>
@endsection
