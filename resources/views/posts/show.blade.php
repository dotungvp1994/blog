@extends('layouts.app-2')
@section('title', $post->title)
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ $post->title }}
            <a class="btn btn-sm btn-default pull-right" href="{{ route('list_post') }}">Return</a>
        </div>

        <div class="panel-body">
            {{ Markdown::parse($post->body) }}
        </div>
    </div>
@endsection