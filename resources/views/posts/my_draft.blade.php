@extends('layouts.app-2')
@section('title', 'My Draft')
@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            @if(isset($posts) && count($posts) > 0)
                @foreach($posts as $post)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3><a href="{{ route('edit_post', ['id' => $post->id]) }}">{{ $post->title }}</a></h3>
                                    <p class="description">{{ str_limit(strip_tags(Markdown::parse($post->body)), 50) }}</p>
                                    <div class="credit" style="min-height: 30px;">
                                        @can('publish-post')
                                            <a href="{{ route('publish_post', ['id' => $post->id]) }}" class="btn btn-default btnPublish" role="button">Publish</a>
                                        @endcan
                                        <a href="{{ route('edit_post', ['id' => $post->id]) }}" class="btn btn-default" role="button">Edit</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="row">
                    <div class="text-center">
                        {{$posts->links()}}
                    </div>
                </div>
            @else
                <h1> List post is empty. Please create new post!!! </h1>
            @endif
        </div>
    </div>
@endsection
@section('body-append')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.btnPublish').click(function (e) {
            e.preventDefault();
            var publishUrl = $(this).attr('href');
            var d = new Date();
            let formatedDate = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes()
            var time = prompt('Fill time want to published post', formatedDate);
            if (Date.parse(time)) {
                $.ajax({
                    type:'GET',
                    url: publishUrl,
                    data: {published_at: time},
                    success:function(data){
                        window.location.reload();
                    }
                });
            } else {
                alert("Time format YYYY-MM-DD HH:mm, please fill again");
            }
        });
    </script>
@endsection