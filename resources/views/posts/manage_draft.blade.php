@extends('layouts.app-2')
@section('title', 'Drafts Post')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Drafts
        </div>

        <div class="panel-body">
            <table class="table table-responsive">
                <thead>
                <td style="width: 50px;" class="text-center">#</td>
                <td class="text-left">Title</td>
                <td style="width: 150px;" class="text-center">Action</td>
                </thead>
                @foreach($posts as $key => $post)
                    <tr>
                        <td class="text-center">{{$key + 1}}</td>
                        <td><a href="{{ route('edit_post', ['id' => $post->id]) }}">{{ $post->title }}</a></td>
                        <td class="text-center">
                            <a href="{{ route('publish_post', ['id' => $post->id]) }}" class="btn btn-sm btn-default btnPublish" role="button">Publish</a>
                        </td>
                    </tr>
                @endforeach
            </table>
            <div style="width: 100%;" class="text-center">
                {{$posts->links()}}
            </div>
        </div>
    </div>
@endsection

@section('body-append')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.btnPublish').click(function (e) {
            e.preventDefault();
            var publishUrl = $(this).attr('href');
            var d = new Date();
            let formatedDate = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes()
            var time = prompt('Fill time want to published post', formatedDate);
            if (Date.parse(time)) {
                $.ajax({
                    type:'GET',
                    url: publishUrl,
                    data: {published_at: time},
                    success:function(data){
                        window.location.reload();
                    }
                });
            } else {
                alert("Time format YYYY-MM-DD HH:mm, please fill again");
            }
        });
    </script>
@endsection