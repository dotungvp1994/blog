<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Title</label>

    <div class="col-md-10">
        <input id="title" type="text" class="form-control" name="title"
               value="{{ old('title') ? old('title') : (isset($post) ? $post->title : '' ) }}"
               required autofocus>

        @if ($errors->has('title'))
            <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
    <label for="body" class="col-md-2 control-label">Body</label>

    <div class="col-md-10">
                        <textarea name="body" id="body" cols="30" rows="10" class="form-control"
                                  required>{!! old('body') ? old('body') : (isset($post) ? $post->body : '' ) !!}</textarea>
        @if ($errors->has('body'))
            <span class="help-block">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
        @endif
    </div>
</div>

<div class="form-group">
    <label for="body" class="col-md-2 control-label">Preview</label>
    <div class="col-md-10">
        <div id="preview" style="min-height: 250px; overflow-y: scroll;" class="form-control"></div>
    </div>
</div>

<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
            {{isset($post) ? 'Update' : 'Create'}}
        </button>
        <a href="{{ route('list_post') }}" class="btn btn-primary">
            Cancel
        </a>
    </div>
</div>