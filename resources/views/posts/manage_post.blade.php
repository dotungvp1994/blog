@extends('layouts.app-2')
@section('title', 'List Post')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Posts
        </div>

        <div class="panel-body">
            <table class="table table-responsive">
                <thead>
                <td style="width: 50px;" class="text-center">#</td>
                <td class="text-left">Title</td>
                <td style="width: 150px;" class="text-center">Action</td>
                </thead>
                @foreach($posts as $key => $post)
                    <tr>
                        <td class="text-center">{{$key + 1}}</td>
                        <td><a href="{{ route('edit_post', ['id' => $post->id]) }}">{{ $post->title }}</a></td>
                        <td class="text-left">
                            <a href="{{ route('edit_post', ['id' => $post->id]) }}" class="btn btn-sm btn-default"
                               role="button">Edit</a>
                            @if(\Auth::user()->can('publish-post') && $post->published == false)
                                <a href="{{ route('publish_post', ['id' => $post->id]) }}"
                                   class="btn btn-sm btn-default btnPublish" role="button">Publish</a>
                            @endif
                            @if(\Auth::user()->can('delete-post'))
                                <a href="{{ route('delete_post', ['id' => $post->id]) }}"
                                   class="btn btn-sm btn-default btnDelete" role="button">Delete</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
            <div style="width: 100%;" class="text-center">
                {{$posts->links()}}
            </div>
        </div>
    </div>
@endsection

@section('body-append')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.btnDelete').click(function (e) {
            e.preventDefault();
            var deleteUrl = $(this).attr('href');
            var r = confirm('Are you sure want to delete post?');
            if (r == true) {
                $.ajax({
                    type:'POST',
                    url: deleteUrl,
                    success:function(data){
                        window.location.reload();
                    }
                });
            }
        });
        $('.btnPublish').click(function (e) {
            e.preventDefault();
            var publishUrl = $(this).attr('href');
            var d = new Date();
            let formatedDate = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes()
            var time = prompt('Fill time want to published post', formatedDate);
            if (Date.parse(time)) {
                $.ajax({
                    type:'GET',
                    url: publishUrl,
                    data: {published_at: time},
                    success:function(data){
                        window.location.reload();
                    }
                });
            } else {
                alert("Time format YYYY-MM-DD HH:mm, please fill again");
            }
        });
    </script>
@endsection