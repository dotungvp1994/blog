@extends('layouts.app-2')
@section('title', 'Home')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">New Post</div>
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('store_post') }}">
                {{ csrf_field() }}
                @include('posts._form')
            </form>
        </div>
    </div>
@endsection

@section('body-append')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/markdown-it/9.0.1/markdown-it.js"></script>
    <script>
        var md = window.markdownit();
        $('#body').blur(function (e) {
            e.preventDefault();
            var previewHtml = md.render($(this).val());
            $('#preview').html('');
            $('#preview').append(previewHtml);
        })
    </script>
@endsection