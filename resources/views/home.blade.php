@extends('layouts.app-2')
@section('title', 'Home')
@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            @forelse($posts as $post)
                <div class="row">
                    <div class="col-md-12">
                        <div class="thumbnail">
                            <div class="caption">
                                <h3><a href="{{ route('show_post', ['id' => $post->id]) }}">{{ $post->title }}</a></h3>
                                <p class="description">{{ str_limit(strip_tags(Markdown::parse($post->body)), 50) }}</p>
                                <div class="credit" style="min-height: 30px;">
                                    <a href="#" class="pull-left">{{$post->author->name}}</a>
                                    <p class="pull-right">{{\Carbon\Carbon::parse($post->published_at, 'UTC')->format('d-m-Y H:s')}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <h1> List post is empty. Please create new post!!! </h1>
            @endforelse
        </div>
    </div>
@endsection
