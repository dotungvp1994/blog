<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'Homecontroller@index');
Route::get('/401.html', function () {
    return view('errors.401');
})->name('errors.401');
Route::get('/posts', 'PostController@index')->name('list_post');

Route::get('/my-post', 'PostController@myPost')
    ->name('my_post')
    ->middleware('auth');
Route::get('/my-draft', 'PostController@myDraft')
    ->name('my_draft')
    ->middleware('auth');
Route::group(['prefix' => 'manage'], function () {
    Route::get('/draft', 'PostController@drafts')
        ->name('manage_draft')
        ->middleware('can:see-all-drafts');
    Route::get('/post', 'PostController@list')
        ->name('manage_post')
        ->middleware('can:see-all-drafts');
});
Route::group(['prefix' => 'posts'], function () {
    Route::get('/show/{id}', 'PostController@show')
        ->name('show_post');
    Route::get('/create', 'PostController@create')
        ->name('create_post')
        ->middleware('can:create-post');
    Route::post('/create', 'PostController@store')
        ->name('store_post')
        ->middleware('can:create-post');
    Route::get('/edit/{post}', 'PostController@edit')
        ->name('edit_post')
        ->middleware('can:update-post,post');
    Route::post('/edit/{post}', 'PostController@update')
        ->name('update_post')
        ->middleware('can:update-post,post');
    Route::get('/publish/{post}', 'PostController@publish')
        ->name('publish_post')
        ->middleware('can:publish-post');
    Route::post('/delete/{post}', 'PostController@destroy')
        ->name('delete_post')
        ->middleware('can:delete-post');
});
